package com.isusdk.onboarding.login;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.isusdk.onboarding.Constants;
import com.isusdk.onboarding.MainActivity;
import com.isusdk.onboarding.R;
import com.isusdk.onboarding.SessionManager;
import com.isusdk.onboarding.configuration.Config;
import com.isusdk.onboarding.configuration.OnBoardSharedPreference;
import com.isusdk.onboarding.forgotpassword.ForgotPasswordActivity;
import com.isusdk.onboarding.otpverification.NewUserActivity;
import com.isusdk.onboarding.otpverification.ResumeProcessActivity;
import com.isusdk.onboarding.otpverification.UserOtpActivity;
import com.isusdk.onboarding.utility.ConnectionDetector;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    TextInputEditText user_name, password;
    Button submit;
    SessionManager session;
    String _user_name, _password;
    ProgressBar progressBar;
    LoginPresenter loginPresenter;
    TextView forgotpassword, newUserCreate;
    SharedPreferences sp;
    public static final String ISU_PREF = "isuPref";
    public static final String USER_NAME = "userNameKey";
    public static final String USER_MPIN = "mpinKey";
    public static final String ADMIN_NAME = "adminNameKey";

    String firestoreAdmin;
    int selected = 0;

    OnBoardSharedPreference preference;
    String appCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sp = getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);
        preference = new OnBoardSharedPreference(this);
        firestoreAdmin = sp.getString(ADMIN_NAME, "");


        Log.e(TAG, "onCreate: admin " + firestoreAdmin);

        newUserCreate = findViewById(R.id.create_new_user);
        user_name = findViewById(R.id.user_name);
        password = findViewById(R.id.password);
        submit = findViewById(R.id.submit);
        progressBar = findViewById(R.id.progressBar);
        forgotpassword = findViewById(R.id.forgotPassword);
        loginPresenter = new LoginPresenter(this);

        if (user_name.hasFocus()) {
            user_name.setCursorVisible(true);
        }

        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);

            }
        });
        // Session Manager
        session = new SessionManager(getApplicationContext());

        newUserCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected = 0;
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Select a process");
                String[] options = {"Resume Process", "Create New User"};
                builder.setSingleChoiceItems(options, selected, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // user checked an item
                        selected = which;
                    }
                });
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int pos) {
                        // user clicked OK
                        preference.clearBoardingPref();
                        Config.setIsNewUser(true);
                        Log.e(TAG, "onClick: " + selected);
                        if (selected == 0) {
                            startActivity(new Intent(LoginActivity.this, ResumeProcessActivity.class));
                        } else if (selected == 1) {
                            startActivity(new Intent(LoginActivity.this, NewUserActivity.class));
                        }
                    }
                });
                builder.setNegativeButton("Cancel", null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

       /* submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionDetector cd = new ConnectionDetector(
                        LoginActivity.this);

//                startActivity(new Intent(LoginActivity.this, AuthMPINActivity.class));

                //throw new RuntimeException("This is a crash test");

                if (cd.isConnectingToInternet()) {
                    _user_name = user_name.getText().toString().trim();
                    _password = password.getText().toString().trim();

                    if (_user_name.length() != 0) {

                        if (_password.length() != 0) {
                            progressBar.setVisibility(View.VISIBLE);
                            loginPresenter.getV1Response("https://itpl.iserveu.tech/generate/v1/");
//                            makeLogin(_user_name, _password);

                        } else {
                            Toast.makeText(LoginActivity.this, "Please enter password", Toast.LENGTH_LONG).show();

                        }
                        //checkSessionExistance(_user_name.toLowerCase());
                    } else {
                        Toast.makeText(LoginActivity.this, "Please enter user name", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
                }

            }
        });*/

        getAdminName(getPackageName());

    }

  /*  private void makeLogin(String id, String pw) {
        JSONObject obj = new JSONObject();

        try {
            obj.put("username", id);
            obj.put("password", pw);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "makeLogin: json " + obj);

        AndroidNetworking.post(Config.getLoginURL())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        Toast.makeText(LoginActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String _token = obj.getString("token");
                            String _admin = obj.getString("adminName");

                            Log.e(TAG, "onResponse: token " + _token);

                            // Use user real data
                            session.createLoginSession(_token, _admin);
                            session.createLoginSession_Username(_user_name);

//                            String app_admin_name = getString(R.string.app_admin_name);

                            List<String> adminList = new ArrayList<>();
                            adminList.add(firestoreAdmin);
                            adminList.add("techadmin");

                            if (!_admin.equalsIgnoreCase(_user_name) && _admin.equalsIgnoreCase(firestoreAdmin)) {
//                            if (!_admin.equalsIgnoreCase(_user_name) && adminList.contains(_admin)) {

                                String oldUser = sp.getString(USER_NAME, "");

                                if (oldUser != null && oldUser.equalsIgnoreCase(_user_name)) {
                                    String mpin = sp.getString(USER_MPIN, "");

                                    if (mpin == null || mpin.length() == 0) {
                                        Intent i = new Intent(getApplicationContext(), AuthMPINActivity.class);
                                        i.putExtra("token", _token);
                                        progressBar.setVisibility(View.INVISIBLE);
                                        startActivity(i);
                                        finish();
                                    } else {
                                        loginWithMpin(_token, mpin);
                                    }
                                } else {
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString(USER_NAME, _user_name);
                                    editor.apply();

                                    Intent i = new Intent(getApplicationContext(), AuthMPINActivity.class);
                                    i.putExtra("token", _token);
                                    progressBar.setVisibility(View.INVISIBLE);
                                    startActivity(i);
                                    finish();
                                }


                            } else {
                                showSessionAlert("Sorry !!! you cannot use application as admin or different admin user, Please check login details or contact our help desk for more information.");
                                progressBar.setVisibility(View.INVISIBLE);
                                // Toast.makeText(LoginActivity.this, "Sorry !!! you cannot use application as admin or different admin user, Please check login details or contact our help desk for more information", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressBar.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "onError: getErrorBody " + anError.getErrorBody());
                        Log.e(TAG, "onError: getErrorDetail " + anError.getErrorDetail());
                        Log.e(TAG, "onError: getResponse " + anError.getResponse());
                        try {
                            preference.clearBoardingPref();
                            preference.setStringValue(Config.USER_NAME_KEY, id);
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());

                            if (errorObject.has("adminName")) {
                                String status = errorObject.getString("status");
                                String adminName = errorObject.getString("adminName");

                                List<String> adminList = new ArrayList<>();
                                adminList.add(firestoreAdmin);
                                adminList.add("techadmin");

                                if (!adminName.equalsIgnoreCase(_user_name) && adminName.equalsIgnoreCase(firestoreAdmin)) {
//                                if (!adminName.equalsIgnoreCase(_user_name) && adminList.contains(adminName)) {
                                    if (status.equals("-1")) {
                                        showNotActivated(errorObject.getString("statusDesc"));
                                        progressBar.setVisibility(View.INVISIBLE);
                                    } else {
//                                        Toast.makeText(LoginActivity.this, errorObject.getString("message"), Toast.LENGTH_LONG).show();
                                        showSessionAlert(errorObject.getString("message"));
                                        progressBar.setVisibility(View.INVISIBLE);
                                    }
                                } else {
                                    showSessionAlert("Sorry !!! you cannot use application as admin or different admin user, Please check login details or contact our help desk for more information.");
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            } else {
                                Toast.makeText(LoginActivity.this, errorObject.getString("message"), Toast.LENGTH_LONG).show();
//                                showSessionAlert(errorObject.getString("message"));
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
//                            showSessionAlert("Incorrect user name and password");
                            Toast.makeText(LoginActivity.this, "Incorrect user name and password", Toast.LENGTH_LONG).show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                });
    }*/


    private void loadLogin(String base64) {

//        base64 = "https://uatapps.iserveu.online/STAGEING/getlogintoken.json?";

        JSONObject obj = new JSONObject();
        try {
            obj.put("username", _user_name);
            obj.put("password", _password);
            AndroidNetworking.post(base64)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());

                                Log.e(TAG, "onResponse: " + obj);

                                String _token = obj.getString("token");
                                String _admin = obj.getString("adminName");

                                // Use user real data
                                session.createLoginSession(_token, _admin);
                                session.createLoginSession_Username(_user_name);
                                progressBar.setVisibility(View.INVISIBLE);

                                Constants.USER_NAME_NOTIFY = _user_name;
//                                String app_admin_name = getString(R.string.app_admin_name);


                                if (!_admin.equalsIgnoreCase(_user_name) && _admin.equalsIgnoreCase(firestoreAdmin)) {
//                                if (_admin.equalsIgnoreCase("demoisu")
//                                        || _admin.equalsIgnoreCase("mposadmin")
//                                        || _admin.equalsIgnoreCase("Ucashnew")
//                                        || _admin.equalsIgnoreCase("dpay")
//                                        || _admin.equalsIgnoreCase("habizw103")
//                                        || _admin.equalsIgnoreCase("atharvw82")
//                                        || _admin.equalsIgnoreCase("indcashw167")
//                                        || _admin.equalsIgnoreCase("ucashapinew")
//                                        || _admin.equalsIgnoreCase("techadmin")
//                                ) {

                                    String oldUser = sp.getString(USER_NAME, "");

                                    if (oldUser != null && oldUser.equalsIgnoreCase(_user_name)) {
                                        String mpin = sp.getString(USER_MPIN, "");

                                        if (mpin == null || mpin.length() == 0) {
                                          /*  Intent i = new Intent(getApplicationContext(), AuthMPINActivity.class);
                                            i.putExtra("token", _token);
                                            progressBar.setVisibility(View.INVISIBLE);
                                            startActivity(i);
                                            finish();*/
                                        } else {
                                            loginWithMpin(_token, mpin);

                                        }
                                    } else {
                                        SharedPreferences.Editor editor = sp.edit();
                                        editor.putString(USER_NAME, _user_name);
                                        editor.apply();

                                       /* Intent i = new Intent(getApplicationContext(), AuthMPINActivity.class);
                                        i.putExtra("token", _token);
                                        progressBar.setVisibility(View.INVISIBLE);
                                        startActivity(i);
                                        finish();*/
                                    }

                                } else {
                                    showSessionAlert("Sorry !!! you cannot use application as admin or different admin user, Please check login details or contact our help desk for more information.");
                                    // Toast.makeText(LoginActivity.this, "Sorry !!! you cannot use application as admin or different admin user, Please check login details or contact our help desk for more information", Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(LoginActivity.this, "Incorrect user name and password", Toast.LENGTH_LONG).show();

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loginWithMpin(String uN, String pin) {

//        ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
//        dialog.setMessage("Loading...");
//        dialog.setCancelable(false);
//        dialog.show();

        String url = "https://us-central1-creditapp-29bf2.cloudfunctions.net/user_mpin/login";
        JSONObject obj = new JSONObject();

        try {
            obj.put("token", uN);
            obj.put("m_pin", pin);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                int status = response.getInt("status");
                                String message = response.getString("message");

                                if (status == 1) {
                                    //load main activity
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString(USER_MPIN, pin);
                                    editor.apply();
                                    progressBar.setVisibility(View.INVISIBLE);

                                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(i);
                                    finish();
                                }
//                                dialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            try {

                                // 0 mismatch
                                // -1 expired
                                // 1 success
                                JSONObject errorObject = new JSONObject(anError.getErrorBody());

                                int status = errorObject.getInt("status");

                                if (status == 0) {
                                    String message = "MPIN has been changed.\nEnter the correct one or regenerate.";
                                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
                                } else if (status == -1) {
                                    String message = "MPIN has been expired.\nRegenrated the MPIN.";
                                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
                                }


                               /* Intent i = new Intent(LoginActivity.this, AuthMPINActivity.class);
                                i.putExtra("token", uN);
                                progressBar.setVisibility(View.INVISIBLE);
                                startActivity(i);
                                finish();*/

                                SharedPreferences.Editor editor = sp.edit();
                                editor.putString(USER_MPIN, "");
                                editor.apply();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressBar.setVisibility(View.INVISIBLE);
                            }
//                            dialog.dismiss();
                        }
                    });


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void fetchedV1Response(boolean status, String response) {
        if (response != null) {
            loadLogin(response);
        }
    }

   /* private String getDeviceIP(){
        try {
            WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
            return Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        }catch (Exception e){
            e.printStackTrace();
        }

    }*/

    private String getDeviceID() {
        return Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }


    private void checkSessionExistance(String user_name) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection("CoreApp_Session_Manager").document(user_name);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if (document.contains("login_status")) {

                                String value = document.getData().get("login_status").toString();

                                if (value.equalsIgnoreCase("false")) {
                                    updateSession(user_name);
                                } else {
                                    showSessionAlert("");
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        updateSession(user_name);
                    }
                } else {

                }
            }
        });
    }

    public void showSessionAlert(String message) {
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(this);
            }
            alertbuilderupdate.setCancelable(false);
            // String message = "Session is already running !!! Please login after sometimes.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(message)
                    .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            progressBar.setVisibility(View.GONE);
                            dialog.dismiss();
//                            getAdminName(getPackageName());
                            //finish();
                        }
                    });

//                    .show();
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        } catch (Exception e) {

        }


    }

    private void updateSession(String user_name) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> map = new HashMap<>();
        map.put("app_unique_id", getPackageName());
        map.put("device_id", getDeviceID());
        map.put("login_datetime", System.currentTimeMillis());
        map.put("login_status", true);
        db.collection("CoreApp_Session_Manager").document(user_name.toLowerCase())
                .set(map)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                        loginPresenter.getV1Response("https://itpl.iserveu.tech/generate/v1/");

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                        loginPresenter.getV1Response("https://itpl.iserveu.tech/generate/v1/");

                    }
                });
    }


    //-------------  For using in SDK ---------------------------------

    public void see_deviceMapping(String userID, String deviceSNo) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("userId", userID);
            obj.put("deviceSlNo", deviceSNo);
            AndroidNetworking.post("https://us-central1-creditapp-29bf2.cloudfunctions.net/isuApi/matmmapping")
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
//                                    progressBar.setVisibility(View.INVISIBLE);
                                if (response.optString("status").equals("1")) {

                                    Toast.makeText(LoginActivity.this, response.optString("desc"), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(LoginActivity.this, response.optString("desc"), Toast.LENGTH_SHORT).show();

                                }

                            } catch (Exception e) {

                            }


                        }

                        @Override
                        public void onError(ANError anError) {
//                                progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(LoginActivity.this, "Error..Please try again  ! ", Toast.LENGTH_LONG).show();

                        }
                    });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void showNotActivated(String statusDesc) {
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(this);
            }
            alertbuilderupdate.setCancelable(false);
            // String message = "Session is already running !!! Please login after sometimes.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(statusDesc)
                    .setPositiveButton("Start EKYC", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            Config.setIsNewUser(false);
                            Intent intent = new Intent(LoginActivity.this, UserOtpActivity.class);
                            intent.putExtra("USER_TYPE", "old");
                            startActivity(intent);
                            dialog.dismiss();
                            //finish();
                        }
                    });
            alertbuilderupdate.setNegativeButton(
                    "Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
//                    .show();
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        } catch (Exception e) {

        }
    }


    public void getAdminName(String name) {
        appCode = "";
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        DocumentReference docRef = db.collection("LiteAppPackage").document(name);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                Log.e(TAG, "onComplete: executed");
                if (task.isSuccessful()) {
                    if (task.getResult().getData() != null) {
                        Log.e(TAG, "onComplete: has data status " + task.getResult().getData());
                        Map<String, Object> packageMap = task.getResult().getData();
                        String appName = (String) packageMap.get("AppName");
                        if (name.equalsIgnoreCase("com.bharatmoney.bharatmoneylite")) {
                            appCode = (String) packageMap.get("AppCode");
                           // String mainApp = (String) packageMap.get("MainApp");

                            if (!appCode.equals("")) {
                              //  String codeString = getString(R.string.app_code);
                               // if (!codeString.equalsIgnoreCase(appCode)) {
                                  //  Intent intent = new Intent(LoginActivity.this, StoreListActivity.class);
                                   // intent.putExtra("appName", appName);
                                    //intent.putExtra("mainApp", name);
                                    //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    //startActivity(intent);
                                    finish();
                               // }
                            }

                        }
                    }
                } else {
                    firestoreAdmin = "";
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString(ADMIN_NAME, firestoreAdmin);
                    editor.apply();
                    Log.e(TAG, "onComplete: error fetching data");
                }
            }
        });
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot, @Nullable FirebaseFirestoreException e) {
                Log.e(TAG, "addSnapshotListener: executed");
                if (e != null) {
                    Log.e(TAG, "Listen failed.", e);
                    return;
                }
                if (snapshot != null && snapshot.exists()) {
                    Log.e(TAG, "Current data: " + snapshot.getData());
                    Map<String, Object> packageMap = snapshot.getData();
                    firestoreAdmin = (String) packageMap.get("AdminName");
                    Config.createBy = firestoreAdmin;


                    //only for com.bharatmoney.bharatmoneylite activate below two lines of codes

                    if (name.equalsIgnoreCase("com.bharatmoney.bharatmoneylitedrive")) {
                        String createdBy = (String) packageMap.get("CreateBy");
                        Config.createBy = createdBy;
                    }


                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString(ADMIN_NAME, firestoreAdmin);
//                    Config.userAdmin = firestoreAdmin;
                    editor.apply();

                    Log.e(TAG, "onEvent: admin name " + Config.userAdmin + " for package " + name);
                    Log.e(TAG, "onEvent: created by name " + Config.createBy + " for package " + name);

                } else {
                    Log.e(TAG, "Current data: null");
                    firestoreAdmin = "";
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString(ADMIN_NAME, firestoreAdmin);
//                    Config.userAdmin = firestoreAdmin;
                    editor.apply();
                }
            }
        });
    }


}
