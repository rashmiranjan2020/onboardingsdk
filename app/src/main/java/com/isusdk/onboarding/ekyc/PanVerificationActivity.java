package com.isusdk.onboarding.ekyc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import com.isusdk.onboarding.R;
import com.isusdk.onboarding.configuration.Config;
import com.isusdk.onboarding.configuration.OnBoardSharedPreference;
import com.isusdk.onboarding.configuration.Validate;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class PanVerificationActivity extends AppCompatActivity {
    private static final String TAG = PanVerificationActivity.class.getSimpleName();
    private EditText panET;
    private ImageView panImage, panSelect, imageEdit;
    private Uri filePath;
    private final int PICK_IMAGE_REQUEST = 71;
    private String pan;
    ProgressDialog dialog;
    OnBoardSharedPreference preference;
    String name, uid, refNumber, imageURL, userName;
    Map<String, Object> document;

    JSONObject docObject;

    FirebaseStorage storage;
    StorageReference storageReference;

    boolean imageAdded = false;
    String sub = "";

    AppCompatCheckBox skipTerms;
    LinearLayout skipLayout, panLayout, buttonLayout;
    Button skip, next, prev;
    boolean skipped = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pan_verification);
        Config.whiteStatusNav(this);
        preference = new OnBoardSharedPreference(this);
        name = preference.getStringValue(Config.NAME_KEY);
        userName = preference.getStringValue(Config.USER_NAME_KEY);
        uid = preference.getStringValue(Config.USER_ID_KEY);
        refNumber = preference.getStringValue(Config.CLIENT_REF_KEY);
        storage = FirebaseStorage.getInstance("gs://iserveu_storage");
        storageReference = storage.getReference();

        document = Config.getDocument();
        docObject = Config.getDocObject();
        Log.e(TAG, "onCreate: docObject" + docObject);

//        Object o = document.get("SUB_STATUS");
//        String sub = (String) o;
        try {
            sub = docObject.getString("SUB_STATUS");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        panSelect = findViewById(R.id.pan_image_select);
        panImage = findViewById(R.id.pan_image);
        imageEdit = findViewById(R.id.pan_image_edit);
        panImage.setVisibility(View.GONE);

        panSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });

        imageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });

        panET = findViewById(R.id.pan_verify);

        //skip feature
        panLayout = findViewById(R.id.pan_layout);
        buttonLayout = findViewById(R.id.pan_button_layout);
        skipLayout = findViewById(R.id.pan_skip_parent);
        skipTerms = findViewById(R.id.pan_skip_term);
        skip = findViewById(R.id.pan_skip);
        next = findViewById(R.id.pan_verify_next);
        prev = findViewById(R.id.pan_verify_prev);
        if (getPackageName().equalsIgnoreCase("com.bharatmoney.bharatmoneylite")) {
            skipLayout.setVisibility(View.VISIBLE);
        } else {
            skipLayout.setVisibility(View.GONE);
        }

        skipTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                skipped = !skipped;
                skipVerification();
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preference.setStringValue(Config.PAN_CARD_KEY, "");
                startActivity(new Intent(PanVerificationActivity.this, EkycRequestActivity.class));
                finish();
            }
        });


        if (sub.equals("2")) {
//            checkReQuery(refNumber);
            storeReQuery();
        } else if (sub.equals("4")) {
//            Map<String, Object> panMap = (Map<String, Object>) document.get("PanInfo");
//            pan = (String) panMap.get("PAN_NUMBER");
//            imageURL = (String) panMap.get("PAN_URL");
            try {
                JSONObject panObject = docObject.getJSONObject("PanInfo");
                pan = panObject.getString("PAN_NUMBER");
                imageURL = panObject.getString("PAN_URL");
                panET.setText(pan);
                Picasso.with(this).load(imageURL).into(panImage);
                panImage.setVisibility(View.VISIBLE);
                imageEdit.setVisibility(View.VISIBLE);
                panSelect.setVisibility(View.GONE);
                imageAdded = true;
//                verifyPan(pan);
//                submitPan(pan);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        panET.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable et) {
                String s = et.toString();
                if (!s.equals(s.toUpperCase())) {
                    s = s.toUpperCase();
                    panET.setText(s);
                    panET.setSelection(panET.length());
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pan = panET.getText().toString();
                if (!imageAdded) {
                    Validate.showAlert(PanVerificationActivity.this, "Please Upload your PAN Image");
                } else if (pan.equals("")) {
                    Validate.showAlert(PanVerificationActivity.this, "Please enter your PAN Card Number");
                } else {
//                    verifyPan(pan);
                    preference.setStringValue(Config.PAN_CARD_KEY, pan);
                    submitPan(pan);
                }
//                if (!pan.equals("")) {
//                    verifyPan(pan);
//                    startActivity(new Intent(PanVerificationActivity.this, EkycRequestActivity.class));
//                } else {
//                    Toast.makeText(PanVerificationActivity.this, "Please enter your PAN Card Number", Toast.LENGTH_LONG).show();
//                }
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        skipVerification();
    }

    //skip feature
    public void skipVerification() {
        skipTerms.setChecked(skipped);
        if (skipped) {
            skip.setEnabled(true);
            skip.setAlpha(1.0f);
            for (int i = 0; i < panLayout.getChildCount(); i++) {
                View child = panLayout.getChildAt(i);
                child.setEnabled(false);
                child.setAlpha(0.2f);
            }
            buttonLayout.setEnabled(true);
            buttonLayout.setAlpha(1.0f);

            prev.setEnabled(true);

            next.setEnabled(false);
            next.setAlpha(0.2f);
            panSelect.setEnabled(false);
            imageEdit.setEnabled(false);
        } else {
            skip.setEnabled(false);
            skip.setAlpha(0.4f);
            for (int i = 0; i < panLayout.getChildCount(); i++) {
                View child = panLayout.getChildAt(i);
                child.setEnabled(true);
                child.setAlpha(1.0f);
            }
            buttonLayout.setEnabled(true);
            buttonLayout.setAlpha(1.0f);

            prev.setEnabled(true);

            next.setEnabled(true);
            next.setAlpha(1.0f);
            panSelect.setEnabled(true);
            imageEdit.setEnabled(true);
        }
    }

    private void storeReQuery() {
        dialog = new ProgressDialog(PanVerificationActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {
            obj.put("type", "REQUERY");
            obj.put("username", preference.getStringValue(Config.USER_NAME_KEY));
            obj.put("clientRefNo", preference.getStringValue(Config.CLIENT_REF_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Config.getSaveUrl())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            String statusDesc = errorObject.getString("statusDesc");
                            Toast.makeText(PanVerificationActivity.this, statusDesc, Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }
                });
    }


    private void checkReQuery(String crfNo) {
        Log.e(TAG, "checkReQuery: check requery " + crfNo);
        dialog = new ProgressDialog(PanVerificationActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        JSONObject obj = new JSONObject();
        try {
            obj.put("username", preference.getStringValue(Config.USER_NAME_KEY));
//            obj.put("requestId", crfNo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(Config.getRequeryURL())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "onResponse: " + response);
                            String status = response.getString("status");
                            if (status.equals("0")) {
                                JSONObject dataObject = response.getJSONObject("data");
                                Map<String, Object> document = Config.getDocument();
                                Map<String, Object> requery = new HashMap<>();
                                requery.put("AREA", dataObject.get("area") + "");
                                requery.put("AUTHENTICATION_CODE", dataObject.get("authenticationcode") + "");
                                requery.put("BUILDING", dataObject.get("building") + "");
                                requery.put("CARE_OF", dataObject.get("careOf") + "");
                                requery.put("CITY", dataObject.get("city") + "");
                                requery.put("COUNTRY", dataObject.get("country") + "");
                                requery.put("DISTRICT", dataObject.get("district") + "");
                                requery.put("DOB", dataObject.get("dob") + "");
                                requery.put("GENDER", dataObject.get("gender") + "");
                                requery.put("NAME", dataObject.get("name") + "");
                                requery.put("PHOTO", dataObject.get("photo") + "");
                                requery.put("PIN", dataObject.get("pin") + "");
                                requery.put("RRN", dataObject.get("rrn") + "");
                                requery.put("STATE", dataObject.get("state") + "");
                                requery.put("STREET", dataObject.get("street") + "");
                                requery.put("TXN_ID", dataObject.get("txnid") + "");
                                requery.put("UIDTOKEN", dataObject.get("uidtoken") + "");
                                requery.put("UNIQUE_REQUEST_ID", dataObject.get("uniquerequestid") + "");
                                requery.put("USER_ID", preference.getStringValue(Config.USER_ID_KEY) + "");
                                requery.put("USER_NAME", preference.getStringValue(Config.USER_NAME_KEY));
                                document.put("kycRequeryInfo", requery);

                                document.put("SUB_STATUS", "3");

                                Log.e(TAG, "onResponse: " + document);

                                FirebaseFirestore db = FirebaseFirestore.getInstance();

                                db.collection("NewUserInfoCollection").document(preference.getStringValue(Config.USER_NAME_KEY))
                                        .set(document)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Log.e(TAG, "DocumentSnapshot successfully written!");
                                                Config.setDocument(document);
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.e(TAG, "Error writing document", e);
                                            }
                                        });
                                dialog.dismiss();
                            } else {
                                Map<String, Object> document = Config.getDocument();
                                Map<String, Object> requery = new HashMap<>();
                                document.put("kycRequeryInfo", requery);
                                document.put("SUB_STATUS", "3");

                                FirebaseFirestore db = FirebaseFirestore.getInstance();
                                db.collection("NewUserInfoCollection").document(preference.getStringValue(Config.USER_NAME_KEY))
                                        .set(document)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Log.e(TAG, "DocumentSnapshot successfully written!");
                                                Config.setDocument(document);
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.e(TAG, "Error writing document", e);
                                            }
                                        });
                                dialog.dismiss();

//                                Toast.makeText(PanVerificationActivity.this, "Please try again", Toast.LENGTH_LONG).show();
//                                finish();
//                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Map<String, Object> document = Config.getDocument();
                            Map<String, Object> requery = new HashMap<>();
                            document.put("kycRequeryInfo", requery);
                            document.put("SUB_STATUS", "3");

                            FirebaseFirestore db = FirebaseFirestore.getInstance();
                            db.collection("NewUserInfoCollection").document(preference.getStringValue(Config.USER_NAME_KEY))
                                    .set(document)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.e(TAG, "DocumentSnapshot successfully written!");
                                            Config.setDocument(document);
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.e(TAG, "Error writing document", e);
                                        }
                                    });
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            String statusDesc = errorObject.getString("statusDesc");
                            Toast.makeText(PanVerificationActivity.this, statusDesc, Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Map<String, Object> document = Config.getDocument();
                        Map<String, Object> requery = new HashMap<>();
                        document.put("kycRequeryInfo", requery);
                        document.put("SUB_STATUS", "3");

                        FirebaseFirestore db = FirebaseFirestore.getInstance();
                        db.collection("NewUserInfoCollection").document(preference.getStringValue(Config.USER_NAME_KEY))
                                .set(document)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.e(TAG, "DocumentSnapshot successfully written!");
                                        Config.setDocument(document);
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.e(TAG, "Error writing document", e);
                                    }
                                });
                        dialog.dismiss();
                    }
                });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            filePath = data.getData();
            String mimeType = getContentResolver().getType(filePath);

            Cursor returnCursor =
                    getContentResolver().query(filePath, null, null, null, null);
            /*
             * Get the column indexes of the data in the Cursor,
             * move to the first row in the Cursor, get the data,
             * and display it.
             */
            int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
            returnCursor.moveToFirst();
            Log.e(TAG, "onActivityResult: cursor " + returnCursor);
            Log.e(TAG, "onActivityResult: name " + returnCursor.getString(nameIndex) + ", " + nameIndex);
            Log.e(TAG, "onActivityResult: size " + returnCursor.getLong(sizeIndex) + ", " + sizeIndex);

            Log.e(TAG, "onActivityResult: check " + (returnCursor.getLong(sizeIndex) < 5000000));


            if (mimeType != null && (mimeType.contains("png") || mimeType.contains("gif") || mimeType.contains("jpg") || mimeType.contains("jpeg"))) {
                if (returnCursor.getLong(sizeIndex) < 5000000) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                        panImage.setImageBitmap(bitmap);
                        panImage.setVisibility(View.VISIBLE);
                        imageEdit.setVisibility(View.VISIBLE);
                        panSelect.setVisibility(View.GONE);
                        imageAdded = true;

                        uploadImage();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Validate.showAlert(PanVerificationActivity.this, "Selected Image size must not be bigger than 5MB.");
                }
            } else {
                Validate.showAlert(PanVerificationActivity.this, "Your PAN Card image must be in png, jpg, jpeg or gif format.");
            }
        }
    }

    private void uploadImage() {
        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StorageReference ref = storageReference.child("KYC_NEW/" + userName + "/" + userName + "_panImg.png");
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();

                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Log.e(TAG, "onSuccess: uri= " + uri.toString());
                                    imageURL = uri.toString();
                                }
                            });

                            Toast.makeText(PanVerificationActivity.this, "Upload successfully", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(PanVerificationActivity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded " + (int) progress + "%");
//                            Log.e(TAG, "onProgress: progress " + progress);
                        }
                    });

        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    private void submitPan(String number) {
        dialog = new ProgressDialog(PanVerificationActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();

        try {
            obj.put("type", "PAN_INFO");
            obj.put("username", preference.getStringValue(Config.USER_NAME_KEY));
            obj.put("pancard", number);
            obj.put("panUrl", imageURL);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "submitPan: request body " + obj);

        AndroidNetworking.post(Config.getSaveUrl())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);

                        startActivity(new Intent(PanVerificationActivity.this, EkycRequestActivity.class));
//                        Config.setDocument(document);
                        dialog.dismiss();
                        finish();
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "onError: " + anError.getErrorDetail());
                        dialog.dismiss();
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            String statusDesc = errorObject.getString("statusDesc");
                            Toast.makeText(PanVerificationActivity.this, statusDesc, Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void verifyPan(String number) {
        //CRPPP5763B
        //GYJPS1843G

        String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));

        dialog = new ProgressDialog(PanVerificationActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        JSONObject obj = new JSONObject();
        JSONArray array = new JSONArray();
        array.put(number);
        try {
            obj.put("pancards", array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "verifyPan: obj " + obj);

        AndroidNetworking.post(Config.getPanURL())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "onResponse: response " + response);
                            JSONArray cardArray = response.getJSONArray("pancards");
                            JSONObject card = cardArray.getJSONObject(0);
                            String panstatus = card.getString("panstatus");
                            if (panstatus.equalsIgnoreCase("E")) {
                                //proceed
                                String statusDesc = response.getString("statusDesc");
                                String filler1 = card.getString("filler1");

                                document = Config.getDocument();
                                Map<String, Object> panMap = new HashMap<>();
                                panMap.put("LAST_UPDATE_DATE", card.getString("last-update-date"));
                                panMap.put("PAN_NAME", filler1);
                                panMap.put("PAN_NUMBER", number);
                                panMap.put("PAN_STATUS", panstatus);
                                panMap.put("PAN_TITLE", card.getString("pan-title"));
                                panMap.put("PAN_URL", imageURL);
                                panMap.put("USER_ID", uid);
                                panMap.put("USER_NAME", preference.getStringValue(Config.USER_NAME_KEY));

                                Date d = new Date();

                                document.put("createdAt", new Date());
                                document.put("updatedAt", new Date());

                                document.put("PanInfo", panMap);
                                document.put("SUB_STATUS", "4");

                                FirebaseFirestore db = FirebaseFirestore.getInstance();
                                db.collection("NewUserInfoCollection").document(preference.getStringValue(Config.USER_NAME_KEY))
                                        .set(document)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Config.setDocument(document);
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(PanVerificationActivity.this, "Error uploading file\nTry again.", Toast.LENGTH_LONG);
                                                Log.e(TAG, "Error writing document", e);
                                            }
                                        });


                                Log.e(TAG, "onResponse: filler " + name + " " + name.equalsIgnoreCase(filler1));
                                if (name.equalsIgnoreCase(filler1)) {
                                    preference.setStringValue(Config.PAN_CARD_KEY, number);

                                    //final firestore
                                    document = Config.getDocument();
                                    document.put("SUB_STATUS", "5");

                                    db = FirebaseFirestore.getInstance();
                                    db.collection("NewUserInfoCollection").document(preference.getStringValue(Config.USER_NAME_KEY))
                                            .set(document)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
//                                                    preference.setStringValue(Config.PAN_KEY);
                                                    Toast.makeText(PanVerificationActivity.this, statusDesc, Toast.LENGTH_LONG).show();
                                                    startActivity(new Intent(PanVerificationActivity.this, EkycRequestActivity.class));
                                                    Config.setDocument(document);
                                                    dialog.dismiss();
                                                    finish();
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Toast.makeText(PanVerificationActivity.this, "Error uploading file\nTry again.", Toast.LENGTH_LONG);
                                                    Log.e(TAG, "Error writing document", e);
                                                }
                                            });

                                } else {
                                    Toast.makeText(PanVerificationActivity.this, "Name mismatched.\nPlease enter own PAN Number", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(PanVerificationActivity.this, "Invalid PAN Number", Toast.LENGTH_LONG).show();
                            }
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            Log.e(TAG, "onError: errorObject" + errorObject);
                            String statusDesc = errorObject.getString("statusDesc");
                            Toast.makeText(PanVerificationActivity.this, statusDesc, Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(PanVerificationActivity.this, "Please try later", Toast.LENGTH_LONG).show();
                        }
                        dialog.dismiss();
                    }
                });
    }

}