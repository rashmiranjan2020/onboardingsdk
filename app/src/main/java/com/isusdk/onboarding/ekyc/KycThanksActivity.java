package com.isusdk.onboarding.ekyc;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.isusdk.onboarding.R;
import com.isusdk.onboarding.configuration.Config;

public class KycThanksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kyc_thanks);
        Config.whiteStatusNav(this);
    }
}