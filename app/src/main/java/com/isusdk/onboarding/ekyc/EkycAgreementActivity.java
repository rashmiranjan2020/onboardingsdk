package com.isusdk.onboarding.ekyc;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import com.isusdk.onboarding.R;
import com.isusdk.onboarding.configuration.Config;


public class EkycAgreementActivity extends AppCompatActivity {
    Button proceed;
    SwitchCompat residenceSwitch, presentSwitch, ageSwitch;
    boolean residence = false, present = false, age = false;
//    boolean residence = true, present = true, age = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ekyc_agreement);
        Config.whiteStatusNav(this);

        residenceSwitch = findViewById(R.id.ekyc_agreement_residence);
        presentSwitch = findViewById(R.id.ekyc_agreement_present);
        ageSwitch = findViewById(R.id.ekyc_agreement_age);
        proceed = findViewById(R.id.ekyc_agreement_proceed);

        residenceSwitch.setChecked(residence);
        presentSwitch.setChecked(present);
        ageSwitch.setChecked(age);

//        activeProceed();

        residenceSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activeProceed();
            }
        });
        presentSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activeProceed();
            }
        });
        ageSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activeProceed();
            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(EkycAgreementActivity.this, EkycFormActivity.class);
                i.putExtra("data", "0");
                startActivity(i);
            }
        });
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        activeProceed();
    }

    public void activeProceed() {
        residence = residenceSwitch.isChecked();
        present = presentSwitch.isChecked();
        age = ageSwitch.isChecked();
        if (residence && present && age) {
            proceed.setEnabled(true);
            proceed.setAlpha(1.0f);
        } else {
            proceed.setEnabled(false);
            proceed.setAlpha(0.5f);
        }
    }

}