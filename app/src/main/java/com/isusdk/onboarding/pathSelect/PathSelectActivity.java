package com.isusdk.onboarding.pathSelect;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isusdk.onboarding.R;
import com.isusdk.onboarding.configuration.Config;
import com.isusdk.onboarding.configuration.OnBoardSharedPreference;
import com.isusdk.onboarding.digilocker.DigilockerActivity;
import com.isusdk.onboarding.ekyc.EkycAgreementActivity;


import org.json.JSONException;
import org.json.JSONObject;

public class PathSelectActivity extends AppCompatActivity {
    private static final String TAG =PathSelectActivity.class.getSimpleName();

    OnBoardSharedPreference preference;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_path_select);
        Config.whiteStatusNav(this);
        preference = new OnBoardSharedPreference(this);

        findViewById(R.id.path_ekyc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PathSelectActivity.this, EkycAgreementActivity.class));
            }
        });

        findViewById(R.id.path_digilocker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = generateName(5);
                Log.e(TAG, "onClick: generated name " + name);

                getDigilocker(name);
            }
        });
    }

    private void getDigilocker(String n) {
        dialog = new ProgressDialog(PathSelectActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        String url = Config.getDigiLockerUrl() + n;
//        String url = Config.getDigiLockerUrl()+preference.getStringValue(Config.USER_NAME_KEY);

        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        try {
                            String digiUrl = response.getString("digiUrl");
                            Intent intent = new Intent(PathSelectActivity.this, DigilockerActivity.class);
                            intent.putExtra("digiUrl", digiUrl);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            Log.e(TAG, "onError: " + errorObject);
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
    }

    public String generateName(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789";
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {
            int index = (int) (AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }

        return sb.toString();
    }

}