package com.isusdk.onboarding.digilocker;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.isusdk.onboarding.R;
import com.isusdk.onboarding.configuration.Config;


public class DigilockerActivity extends AppCompatActivity {
    private static final String TAG = DigilockerActivity.class.getSimpleName();
    WebView webView;
    TextView textView;

    boolean complete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digilocker);
        Config.whiteStatusNav(this);

        complete = false;
        webView = findViewById(R.id.digilocker_webview);
        textView = findViewById(R.id.digi_text);

        String url = getIntent().getStringExtra("digiUrl");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                Log.e(TAG, "onPageStarted: loading " + url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Log.e(TAG, "onPageFinished: finish " + url);
                super.onPageFinished(view, url);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onLoadResource(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.e(TAG, "shouldOverrideUrlLoading: clicked on " + url);
                if (url.contains("digilocker/android")) {
                    webView.setVisibility(View.GONE);
                    Log.e(TAG, "shouldOverrideUrlLoading: now fetch data" );
                    textView.setVisibility(View.VISIBLE);
                    complete = true;

                }
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        webView.loadUrl(url);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.cancelLongPress();

    }

    @Override
    public void onBackPressed() {
        if (complete){
            finish();
        } else {
            if (webView.canGoBack()) {
                webView.goBack();
            } else {
                super.onBackPressed();
            }
        }
    }
}