package com.isusdk.onboarding.otpverification;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.transition.TransitionManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.isusdk.onboarding.R;
import com.isusdk.onboarding.configuration.Config;
import com.isusdk.onboarding.configuration.OnBoardSharedPreference;
import com.isusdk.onboarding.configuration.Validate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserOtpActivity extends AppCompatActivity {
    private static final String TAG = UserOtpActivity.class.getSimpleName();
    Button proceed;
    EditText mobileET, otpET;
    String mobile, otp;
    LinearLayout parent;
    boolean otpVerified = false;
    ProgressDialog dialog;
    List<String> userNameFS;
    boolean own;
    OnBoardSharedPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_otp);
        Config.whiteStatusNav(this);

        if (Config.isIsNewUser()){
            getFrestoreCollection();
        }


        preference = new OnBoardSharedPreference(this);
        Log.e(TAG, "onCreate: " + preference.getStringValue(Config.PHONE_KEY));
//        if (!preference.getStringValue(Config.PHONE_KEY).equals("")) {
//            startActivity(new Intent(UserOtpActivity.this, WelcomeActivity.class));
//            finish();
//        }

        parent = findViewById(R.id.login_parent);
        proceed = findViewById(R.id.login_proceed);
        mobileET = findViewById(R.id.login_mobile);
        otpET = findViewById(R.id.login_otp);

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (otpVerified) {
                    otp = otpET.getText().toString();
                    if (!otp.equals("")) {
                        verifyOtp(mobile, otp);
                    } else {
                        hideKeyboard();
                        Validate.showAlert(UserOtpActivity.this, "Enter the OTP");
                    }
                } else {
                    mobile = mobileET.getText().toString();
                    if (Validate.isValidPhone(mobile)) {
//                    if (!mobile.equals("")) {
                        mobile = Validate.getValidPhone(mobile);
                        sendOTP(mobile);
                    } else {
                        hideKeyboard();
                        Validate.showAlert(UserOtpActivity.this, "Enter a valid mobile number");
                    }
                }
            }
        }
        );
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void verifyOtp(final String m, String o) {
        dialog = new ProgressDialog(UserOtpActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        JSONObject obj = new JSONObject();

        try {
            obj.put("phoneNumber", m);
            obj.put("otp", o);
            obj.put("type", "phone");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Config.getOtpURL())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: response " + response);
                        try {
                            int statusCode = response.getInt("statusCode");
                            if (statusCode == 0) {
                                JSONObject data = response.getJSONObject("data");
                                String status = data.getString("status");
                                if (status.equalsIgnoreCase("success")) {
                                    String statusDesc = data.getString("statusDesc");
                                    preference.setStringValue(Config.PHONE_KEY, m);
                                    preference.setStringValue(Config.OTP_KEY, o);
//                                    Toast.makeText(UserOtpActivity.this, statusDesc, Toast.LENGTH_LONG).show();
//                                    if (Config.isIsNewUser()){
//                                        Intent intent = new Intent(UserOtpActivity.this, VerifyEmailActivity.class);
//                                        intent.putExtra("USER_TYPE", "new");
//                                        startActivity(intent);
//                                        finish();
//                                    } else {
//
//                                    }

                                    if (Config.isIsNewUser()) {
                                        if (userNameFS.contains(preference.getStringValue(Config.USER_NAME_KEY))) {
                                            showAlert(UserOtpActivity.this, "User Name has been taken already. Please try again later.");
                                        } else {
                                            addToFireStore(preference.getStringValue(Config.USER_NAME_KEY));
                                        }
                                    } else {
                                        Intent intent = new Intent(UserOtpActivity.this, WelcomeActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }

                                } else {
                                    Toast.makeText(UserOtpActivity.this, "Try Again", Toast.LENGTH_LONG).show();
                                }
                            }

                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {

                            // 0 mismatch
                            // -1 expired
                            // 1 success
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            Log.e(TAG, "onError: response " + errorObject);
                            JSONObject data = errorObject.getJSONObject("data");
                            String statusDesc = data.getString("statusDesc");
                            Toast.makeText(UserOtpActivity.this, statusDesc, Toast.LENGTH_LONG).show();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                });


    }

    private void getFrestoreCollection() {
        own = false;
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference uNames = db.collection("SelfUserNames");
//        uNames.get()
//                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                    @Override
//                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
//                        if (task.isSuccessful()) {
//                            Log.e(TAG, "onComplete: " + task.getResult());
//                            Log.e(TAG, "onComplete: " + task.getResult().getDocuments());
//                            for (QueryDocumentSnapshot document : task.getResult()) {
//                                Log.e(TAG, document.getId() + " => " + document.getData());
//                            }
//                        } else {
//                            Log.e(TAG, "Error getting documents: ", task.getException());
//                        }
//                    }
//                });

        uNames.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.e(TAG, "onEvent: error occured");
                }

                if (snapshots.isEmpty()) {
                    Log.e(TAG, "onEvent: empty snap");
                } else {
                    List<DocumentSnapshot> doc = snapshots.getDocuments();
                    userNameFS = new ArrayList<>();
                    for (int i = 0; i < doc.size(); i++) {
                        Map<String, Object> userDetails = doc.get(i).getData();
                        String st = (String) userDetails.get("status");
                        if (st.equals("0")) {
                            userNameFS.add(doc.get(i).getId());
                        }
                        Log.e(TAG, doc.get(i).getId() + " => " + doc.get(i).getData());
                    }

                    if (userNameFS.contains(preference.getStringValue(Config.USER_NAME_KEY)) && !own) {
                        showAlert(UserOtpActivity.this, "User Name has been taken already. Please try again later.");
                    }

                }
            }
        });
    }

    private void addToFireStore(String u) {

        own = true;

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference uNames = db.collection("SelfUserNames");

        Map<String, Object> userDetails = new HashMap<>();
        userDetails.put("password", preference.getStringValue(Config.USER_PASSWORD_KEY));
        userDetails.put("phone", preference.getStringValue(Config.PHONE_KEY));
        userDetails.put("admin", Config.userAdmin);
        userDetails.put("status", "0");
//        userDetails.put("admin", "demoisu");

        uNames.document(u).set(userDetails)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e(TAG, "DocumentSnapshot successfully written!" + userDetails);
                        Intent intent = new Intent(UserOtpActivity.this, WelcomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "Error writing document", e);
                Toast.makeText(UserOtpActivity.this, "Try Again Later!", Toast.LENGTH_LONG).show();
            }
        });


    }

    private void sendOTP(String p) {
        dialog = new ProgressDialog(UserOtpActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();

        try {
            obj.put("phoneNumber", p);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Config.getOtpURL())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int statusCode = response.getInt("statusCode");
                            if (statusCode == 0) {
                                JSONObject data = response.getJSONObject("data");
                                String status = data.getString("status");
                                if (status.equalsIgnoreCase("success")) {
                                    String statusDesc = data.getString("statusDesc");
                                    Toast.makeText(UserOtpActivity.this, statusDesc, Toast.LENGTH_LONG).show();
                                    otpVerified = true;
                                    TransitionManager.beginDelayedTransition(parent);
                                    proceed.setText("PROCEED");
                                    otpET.setVisibility(View.VISIBLE);
                                    mobileET.setEnabled(false);
                                } else {
                                    Toast.makeText(UserOtpActivity.this, "Try Again", Toast.LENGTH_LONG).show();
                                }
                            }

                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {

                            // 0 mismatch
                            // -1 expired
                            // 1 success
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            JSONObject data = errorObject.getJSONObject("data");
                            String statusDesc = data.getString("statusDesc");
                            Toast.makeText(UserOtpActivity.this, statusDesc, Toast.LENGTH_LONG).show();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                });
    }

    void showAlert(Context context, String statusDesc) {
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(context);
            }
            alertbuilderupdate.setCancelable(false);
            // String message = "Session is already running !!! Please login after sometimes.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(statusDesc)
                    .setPositiveButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                            finish();
                        }
                    });

//                    .show();
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        } catch (Exception e) {

        }
    }

}