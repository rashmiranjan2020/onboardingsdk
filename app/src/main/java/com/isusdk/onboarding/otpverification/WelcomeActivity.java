package com.isusdk.onboarding.otpverification;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isusdk.onboarding.R;
import com.isusdk.onboarding.configuration.Config;
import com.isusdk.onboarding.configuration.OnBoardSharedPreference;
import com.isusdk.onboarding.ekyc.EkycFormActivity;
import com.isusdk.onboarding.ekyc.EkycRequestActivity;
import com.isusdk.onboarding.ekyc.PanVerificationActivity;
import com.isusdk.onboarding.login.LoginActivity;
import com.isusdk.onboarding.pathSelect.PathSelectActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class WelcomeActivity extends AppCompatActivity {
    private static final String TAG = WelcomeActivity.class.getSimpleName();
    ProgressDialog dialog;
    OnBoardSharedPreference preference;
    String phone;
    Map<String, Object> mapObject = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        Config.whiteStatusNav(this);

        preference = new OnBoardSharedPreference(this);
        phone = preference.getStringValue(Config.PHONE_KEY);

        String u = preference.getStringValue(Config.USER_NAME_KEY);
        Log.e(TAG, "onCreate: user name key " + u);
//        u = "itpl";

        FirebaseFirestore db = FirebaseFirestore.getInstance();
//        Map<String, Object> mapObject = Config.getFirestoreData(this, db, u);

        /*
        DocumentReference docRef = db.collection("NewUserInfoCollection").document(u);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    if (task.getResult().getData() != null) {
                        Log.e(TAG, "onComplete: has data status " + task.getResult().getData());
                        mapObject = task.getResult().getData();
                        Log.e(TAG, "onComplete: map " + mapObject);
                        Config.setDocument(mapObject);
                    } else {
                        mapObject = null;
                    }
                } else {
                    Log.e(TAG, "onComplete: error fetching data");
                }
            }

        });

        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Log.e(TAG, "onSuccess: " + documentSnapshot.getData());
            }
        });
        */

        findViewById(R.id.welcome_proceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.e(TAG, "onClick: empty map "+mapObject.isEmpty() );
//                generateUserID();
                kycFetch(preference.getStringValue(Config.USER_NAME_KEY));
            }
        });
    }

    private void kycFetch(String name) {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {
            obj.put("username", name);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Config.getFetchUrl())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        try {
                            String status = response.getString("status");
                            if (status.equals("0")) {
                                JSONObject data = response.getJSONObject("data");
                                String subStatus = data.getString("SUB_STATUS");
                                Config.setDocObject(data);
                                if (data.has("basicInfo")) {
                                    Log.e(TAG, "onResponse: data has info " + data.getJSONObject("basicInfo"));
                                    JSONObject basicInfo = data.getJSONObject("basicInfo");
                                    String name = basicInfo.getString("NAME");
                                    String phone = basicInfo.getString("PHONE");
                                    String email = basicInfo.getString("EMAIL");
                                    String city = basicInfo.getString("CITY");
                                    String state = basicInfo.getString("STATE");
                                    String address = basicInfo.getString("ADDRESS");
                                    String adharCard = basicInfo.getString("AADHAAR");
                                    String shopName = basicInfo.getString("SHOP");
                                    String pincode = basicInfo.getString("PINCODE");

                                    String surName = name.split(" ")[name.split(" ").length - 1];
                                    String firstName = name.substring(0, name.length() - surName.length()).trim();
                                    if (surName.equals("")){
                                        surName = "N/A";
                                    }
                                    preference.setStringValue(Config.FIRST_NAME_KEY, firstName);
                                    preference.setStringValue(Config.LAST_NAME_KEY, surName);
                                    preference.setStringValue(Config.SHOP_KEY, shopName);
                                    preference.setStringValue(Config.AADHAAR_KEY, adharCard);
                                    preference.setStringValue(Config.MAIL_KEY, email);
                                    preference.setStringValue(Config.CITY_KEY, city);
                                    preference.setStringValue(Config.STATE_KEY, state);
                                    preference.setStringValue(Config.ADDRESS_KEY, address);
                                    preference.setStringValue(Config.PIN_KEY, pincode);

                                }
                                if (subStatus.equals("1")) {
                                    // status == 1, skip basic details and load rbl link
                                    Intent i = new Intent(WelcomeActivity.this, EkycFormActivity.class);
                                    i.putExtra("data", "1");


//                                    Intent i = new Intent(WelcomeActivity.this, RblLinkActivity.class);
//                                    String cpuniquerefno = data.getString("cpuniquerefno");
//                                    String url = data.getString("url");
//                                    i.putExtra("url", url + "&cpuniquerefno=" + cpuniquerefno);

//                                    i.putExtra("data", "1");

                                    Log.e(TAG, "onResponse: data " + data);
                                    dialog.dismiss();
                                    startActivity(i);
                                } else if (subStatus.equals("2")) {
                                    // status == 2, skip rbl url and get requery and set

                                    String clientRefNo = data.getString("clientRefNo");
                                    preference.setStringValue(Config.CLIENT_REF_KEY, clientRefNo);

                                    JSONObject basicInfo = data.getJSONObject("basicInfo");
                                    String mapName = basicInfo.getString("NAME");
                                    preference.setStringValue(Config.NAME_KEY, mapName);

                                    Intent i = new Intent(WelcomeActivity.this, PanVerificationActivity.class);
                                    dialog.dismiss();
                                    startActivity(i);
                                } else if (subStatus.equals("3")) {
                                    // status == 3, load pan info activity,
                                    JSONObject basicInfo = data.getJSONObject("basicInfo");
                                    String mapName = basicInfo.getString("NAME");
                                    preference.setStringValue(Config.NAME_KEY, mapName);

                                    Intent intent = new Intent(WelcomeActivity.this, PanVerificationActivity.class);
                                    dialog.dismiss();
                                    startActivity(intent);
                                } else if (subStatus.equals("4")) {
                                    // status == 4, get pan info from firestore, try matching
                                    JSONObject basicInfo = data.getJSONObject("basicInfo");
                                    String mapName = basicInfo.getString("NAME");
                                    preference.setStringValue(Config.NAME_KEY, mapName);

                                    Intent intent = new Intent(WelcomeActivity.this, PanVerificationActivity.class);
                                    dialog.dismiss();
                                    startActivity(intent);
                                } else if (subStatus.equals("5")) {
                                    // status == 5, load kyc request and complete kyc
                                    JSONObject panObject = data.getJSONObject("PanInfo");
                                    String pan = panObject.getString("PAN_NUMBER");
                                    preference.setStringValue(Config.PAN_CARD_KEY, pan);
                                    Intent intent = new Intent(WelcomeActivity.this, EkycRequestActivity.class);
                                    dialog.dismiss();
                                    startActivity(intent);
                                } else if (subStatus.equals("6")){

                                    String msg = "EKYC process has been completed for the user " + preference.getStringValue(Config.USER_NAME_KEY) +
                                            "\nPlease make the login";
                                    Toast.makeText(WelcomeActivity.this, msg, Toast.LENGTH_LONG).show();
                                }
                            }
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        startActivity(new Intent(WelcomeActivity.this, PathSelectActivity.class));
                        dialog.dismiss();
                    }
                });


    }

    private void generateUserID() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {
            JSONObject data = new JSONObject();
            data.put("name", "iServeU");
            data.put("user_name", preference.getStringValue(Config.USER_NAME_KEY));
            data.put("phone", phone);
            obj.put("dataInfo", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(Config.getUserIdURL())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            String message = response.getString("message");
//                            Toast.makeText(WelcomeActivity.this, message, Toast.LENGTH_LONG).show();
                            if (status.equals("0")) {
                                String link = response.getString("link");
                                Log.e(TAG, "onResponse: link " + link);
                                if (link.contains("user_id")) {
                                    int index = link.indexOf("user_id=") + "user_id".length() + 1;
                                    String userId = link.substring(index);
                                    Log.e(TAG, "onResponse: " + userId);
                                    preference.setStringValue(Config.USER_ID_KEY, userId);
                                    // mapobject = null >> path select
                                    if (mapObject == null) {
                                        startActivity(new Intent(WelcomeActivity.this, PathSelectActivity.class));
                                        dialog.dismiss();
                                    } else {
                                        Object o = mapObject.get("SUB_STATUS");
                                        String sStatus = (String) o;

                                        // status == 1, skip basic details and load rbl link
                                        if (sStatus.equals("1")) {
                                            Intent i = new Intent(WelcomeActivity.this, EkycFormActivity.class);
                                            i.putExtra("data", "1");
                                            dialog.dismiss();
                                            startActivity(i);
                                        } else if (sStatus.equals("2")) {
                                            // status == 2, skip rbl url and get requery and set
                                            Map<String, Object> basicInfoMap = (Map<String, Object>) mapObject.get("basicInfo");
                                            String mapName = (String) basicInfoMap.get("NAME");
                                            preference.setStringValue(Config.NAME_KEY, mapName);

                                            Intent i = new Intent(WelcomeActivity.this, PanVerificationActivity.class);
                                            dialog.dismiss();
                                            startActivity(i);
                                        } else if (sStatus.equals("3")) {
                                            // status == 3, load pan info activity,
                                            Map<String, Object> basicInfoMap = (Map<String, Object>) mapObject.get("basicInfo");
                                            String mapName = (String) basicInfoMap.get("NAME");
                                            preference.setStringValue(Config.NAME_KEY, mapName);

                                            Intent intent = new Intent(WelcomeActivity.this, PanVerificationActivity.class);
                                            dialog.dismiss();
                                            startActivity(intent);
                                        } else if (sStatus.equals("4")) {
                                            // status == 4, get pan info from firestore, try matching
                                            Map<String, Object> basicInfoMap = (Map<String, Object>) mapObject.get("basicInfo");
                                            String mapName = (String) basicInfoMap.get("NAME");
                                            preference.setStringValue(Config.NAME_KEY, mapName);

                                            Intent intent = new Intent(WelcomeActivity.this, PanVerificationActivity.class);
                                            dialog.dismiss();
                                            startActivity(intent);
                                        } else if (sStatus.equals("5")) {
                                            // status == 5, load kyc request and complete kyc
                                            Intent intent = new Intent(WelcomeActivity.this, EkycRequestActivity.class);
                                            dialog.dismiss();
                                            startActivity(intent);
                                        } else if (sStatus.equals("6")){
                                            Toast.makeText(WelcomeActivity.this, "User name already exists. Please make the login", Toast.LENGTH_LONG).show();
                                            Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        }
                                    }
                                } else {
                                    Toast.makeText(WelcomeActivity.this, "Try Again.", Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            Toast.makeText(WelcomeActivity.this, errorObject.toString(), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG, "onError: exception " + e);
                        }
                        dialog.dismiss();
                    }
                });
    }

    private String getClientRefNo(String value) {
        StringBuilder v = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            v.append((int) value.charAt(i));
        }
        v.append(System.currentTimeMillis());
        return v.toString();
    }
}